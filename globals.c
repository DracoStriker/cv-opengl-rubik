/*
 * callbacks.c
 *
 * Sim�o Reis - Nov/2013
 * Miguel Val�rio - Nov/2013
 *
 */

#include "globals.h"

/* Alus�o �s fun��es auxiliares. */

static void swap(pontModelo *, pontModelo *);

/* Verificar se o cubo est� resolvido atrav�s das cores */

int rubikSolved()
{
    int i;

    for(i = 0; i < 8; i++)
    {
        if((arrCores[rotations[XX_AXIS][2][i]][1*18  ] != arrCores[rotations[YY_AXIS][1][2]][1*18  ]) ||
           (arrCores[rotations[XX_AXIS][2][i]][1*18+1] != arrCores[rotations[YY_AXIS][1][2]][1*18+1]) ||
           (arrCores[rotations[XX_AXIS][2][i]][1*18+2] != arrCores[rotations[YY_AXIS][1][2]][1*18+2]) ||
           (arrCores[rotations[XX_AXIS][0][i]][3*18  ] != arrCores[rotations[YY_AXIS][1][7]][3*18  ]) ||
           (arrCores[rotations[XX_AXIS][0][i]][3*18+1] != arrCores[rotations[YY_AXIS][1][7]][3*18+1]) ||
           (arrCores[rotations[XX_AXIS][0][i]][3*18+2] != arrCores[rotations[YY_AXIS][1][7]][3*18+2]) ||
           (arrCores[rotations[YY_AXIS][2][i]][4*18  ] != arrCores[rotations[XX_AXIS][1][7]][4*18  ]) ||
           (arrCores[rotations[YY_AXIS][2][i]][4*18+1] != arrCores[rotations[XX_AXIS][1][7]][4*18+1]) ||
           (arrCores[rotations[YY_AXIS][2][i]][4*18+2] != arrCores[rotations[XX_AXIS][1][7]][4*18+2]) ||
           (arrCores[rotations[YY_AXIS][0][i]][5*18  ] != arrCores[rotations[XX_AXIS][1][3]][5*18  ]) ||
           (arrCores[rotations[YY_AXIS][0][i]][5*18+1] != arrCores[rotations[XX_AXIS][1][3]][5*18+1]) ||
           (arrCores[rotations[YY_AXIS][0][i]][5*18+2] != arrCores[rotations[XX_AXIS][1][3]][5*18+2]) ||
           (arrCores[rotations[ZZ_AXIS][0][i]][2*18  ] != arrCores[rotations[YY_AXIS][1][5]][2*18  ]) ||
           (arrCores[rotations[ZZ_AXIS][0][i]][2*18+1] != arrCores[rotations[YY_AXIS][1][5]][2*18+1]) ||
           (arrCores[rotations[ZZ_AXIS][0][i]][2*18+2] != arrCores[rotations[YY_AXIS][1][5]][2*18+2]) ||
           (arrCores[rotations[ZZ_AXIS][2][i]][0*18  ] != arrCores[rotations[YY_AXIS][1][1]][0*18  ]) ||
           (arrCores[rotations[ZZ_AXIS][2][i]][0*18+1] != arrCores[rotations[YY_AXIS][1][1]][0*18+1]) ||
           (arrCores[rotations[ZZ_AXIS][2][i]][0*18+2] != arrCores[rotations[YY_AXIS][1][1]][0*18+2]))
        {
            return 0;
        }
    }

    return 1;
}

/* Rota��o topol�gica do rubik. (reorganizar a matriz) */

void rotateRubik(int dir)
{
    switch( dir )
    {
        case CLOCKWISE :

            switch ( axis )
            {
                case XX_AXIS :

                    swap(&rubik[num][0][2],&rubik[num][0][0]);

                    swap(&rubik[num][1][2],&rubik[num][0][1]);

                    swap(&rubik[num][0][0],&rubik[num][2][0]);

                    swap(&rubik[num][0][1],&rubik[num][1][0]);

                    swap(&rubik[num][2][0],&rubik[num][2][2]);

                    swap(&rubik[num][1][0],&rubik[num][2][1]);

                    break;

                case YY_AXIS :

                    swap(&rubik[0][num][2],&rubik[2][num][2]);

                    swap(&rubik[1][num][2],&rubik[2][num][1]);

                    swap(&rubik[2][num][2],&rubik[2][num][0]);

                    swap(&rubik[2][num][1],&rubik[1][num][0]);

                    swap(&rubik[2][num][0],&rubik[0][num][0]);

                    swap(&rubik[1][num][0],&rubik[0][num][1]);

                    break;

                case ZZ_AXIS :

                    swap(&rubik[0][2][num],&rubik[0][0][num]);

                    swap(&rubik[1][2][num],&rubik[0][1][num]);

                    swap(&rubik[0][0][num],&rubik[2][0][num]);

                    swap(&rubik[0][1][num],&rubik[1][0][num]);

                    swap(&rubik[2][0][num],&rubik[2][2][num]);

                    swap(&rubik[1][0][num],&rubik[2][1][num]);

                    break;
            }

            break;

        case ANTICLOCKWISE :

            switch ( axis )
            {
                case XX_AXIS :

                    swap(&rubik[num][0][2],&rubik[num][2][2]);

                    swap(&rubik[num][1][2],&rubik[num][2][1]);

                    swap(&rubik[num][2][2],&rubik[num][2][0]);

                    swap(&rubik[num][2][1],&rubik[num][1][0]);

                    swap(&rubik[num][2][0],&rubik[num][0][0]);

                    swap(&rubik[num][1][0],&rubik[num][0][1]);

                    break;

                case YY_AXIS :

                    swap(&rubik[0][num][2],&rubik[0][num][0]);

                    swap(&rubik[1][num][2],&rubik[0][num][1]);

                    swap(&rubik[0][num][0],&rubik[2][num][0]);

                    swap(&rubik[0][num][1],&rubik[1][num][0]);

                    swap(&rubik[2][num][0],&rubik[2][num][2]);

                    swap(&rubik[1][num][0],&rubik[2][num][1]);

                    break;

                case ZZ_AXIS :

                    swap(&rubik[0][2][num],&rubik[2][2][num]);

                    swap(&rubik[1][2][num],&rubik[2][1][num]);

                    swap(&rubik[2][2][num],&rubik[2][0][num]);

                    swap(&rubik[2][1][num],&rubik[1][0][num]);

                    swap(&rubik[2][0][num],&rubik[0][0][num]);

                    swap(&rubik[1][0][num],&rubik[0][1][num]);

                    break;
            }

            break;
    }
}

/* Fun��o auxiliar para realizar as rota��es topologicas */

static void swap(pontModelo *m1, pontModelo *m2)
{
    pontModelo temp = *m1;
    *m1 = *m2;
    *m2 = temp;
}
