/*
 * init.h
 *
 * Ficheiro cabecalho do modulo INIT.
 *
 * J. Madeira - Out/2012
 */


#ifndef _init_h
#define _init_h


void inicializarEstado( void );

void inicializarJanela( void );

void inicializarFontesDeLuz( void );

void inicializarModelos( void );

void inicializarRubik ( void );

void libertarArraysModelo( int i );

void libertarModelos( void );


#endif
