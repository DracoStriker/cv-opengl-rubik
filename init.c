/*
 * init.c
 *
 * Ficheiro de implementacao do modulo INIT.
 *
 * J. Madeira - Out/2012
 * Sim�o Reis - Nov/2013
 * Miguel Val�rio - Nov/2013
 *
 */


#include <stdlib.h>

#include <stdio.h>

#include <string.h>


#define GLEW_STATIC /* Necessario se houver problemas com a lib */

#include <GL/glew.h>

#include <GL/freeglut.h>


#include "globals.h"

#include "mathUtils.h"

#include "models.h"

#include "callbacks.h"


/* Centro de massa dos cubos do rubik. */

static float desl[27][3] =
{
    {
        -1.6, -1.6, -1.6
    },
    {
        0.0, -1.6, -1.6
    },
    {
        1.6, -1.6, -1.6
    },
    {
        -1.6, 0.0, -1.6
    },
    {
        0.0, 0.0, -1.6
    },
    {
        1.6, 0.0, -1.6
    },
    {
        -1.6, 1.6, -1.6
    },
    {
        0.0, 1.6, -1.6
    },
    {
        1.6, 1.6, -1.6
    },
    {
        -1.6, -1.6, 0.0
    },
    {
        0.0, -1.6, 0.0
    },
    {
        1.6, -1.6, 0.0
    },
    {
        -1.6, 0.0, 0.0
    },
    {
        0.0, 0.0, 0.0
    },
    {
        1.6, 0.0, 0.0
    },
    {
        -1.6, 1.6, 0.0
    },
    {
        0.0, 1.6, 0.0
    },
    {
        1.6, 1.6, 0.0
    },
    {
        -1.6, -1.6, 1.6
    },
    {
        0.0, -1.6, 1.6
    },
    {
        1.6, -1.6, 1.6
    },
    {
        -1.6, 0.0, 1.6
    },
    {
        0.0, 0.0, 1.6
    },
    {
        1.6, 0.0, 1.6
    },
    {
        -1.6, 1.6, 1.6
    },
    {
        0.0, 1.6, 1.6
    },
    {
        1.6, 1.6, 1.6
    }
};


void inicializarEstado( void )
{

   /* DOUBLE-BUFFERING + DEPTH-TESTING */

   glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );

   /* Definir a cor do fundo */

   glClearColor( 0.0, 0.0, 0.0, 1.0 );

   /* Atributos das primitivas */

   glPointSize( 4.0 );

   glLineWidth( 3.0 );

   /* Modo de desenho dos poligonos */

   glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

   /* Back-Face Culling */

   glCullFace( GL_BACK );

   glFrontFace( GL_CCW );

   glEnable( GL_CULL_FACE );

   /* Depth-Buffer */

   glEnable( GL_DEPTH_TEST );

   /* Matriz de projeccao � inicialmente a IDENTIDADE => Proj. Paralela Ortogonal */

   matrizProj = CreateProjectionMatrix( 60, 16.0/9.0, 0.01, 40 );

   matrizModelView = IDENTITY_MATRIX;

   /* Para rodar globalmente a cena */

   animacaoCenaON = 0;

   animacaoModelosON = 0;

   animacaoFocosON = 0;

   setOrigin = 1;

   orgX = orgY = rotX = rotY = 0.0;
}


void inicializarJanela( void )
{
   /* Caracteristicas da janela de saida */

   glutInitWindowSize( 800, 450 ); /* Usar variaveis GLOBAIS para estes parametros */

   glutInitWindowPosition( 100, 100 );

   /* Para terminar de modo apropriado */

   glutSetOption(

        GLUT_ACTION_ON_WINDOW_CLOSE,

        GLUT_ACTION_GLUTMAINLOOP_RETURNS

    );

   /* Criar a janela de saida */

   windowHandle = glutCreateWindow( "Rubik Cube" );

   if( windowHandle < 1 )
   {
        fprintf(

             stderr,

             "ERROR: Could not create a new rendering window.\n"

         );

         exit( EXIT_FAILURE );
   }
}

void inicializarFontesDeLuz( void )
{
    numFocos = 1;

    /* Criar o array */

    arrayFocos = (pontFoco*) malloc( numFocos * sizeof(pontFoco) );

    /* Foco 0 */

    arrayFocos[0] = (pontFoco) malloc( sizeof(Registo_Foco) );

    arrayFocos[0]->focoIsOn = 1;

    arrayFocos[0]->posicao[0] = 0.0;

    arrayFocos[0]->posicao[1] = 0.0;

    arrayFocos[0]->posicao[2] = 5.0;

    arrayFocos[0]->posicao[3] = 1.0; /* Foco PONTUAL */

    arrayFocos[0]->intensidade[0] = 1.0;

    arrayFocos[0]->intensidade[1] = 1.0;

    arrayFocos[0]->intensidade[2] = 1.0;

    arrayFocos[0]->intensidade[3] = 1.0;

    arrayFocos[0]->luzAmbiente[0] = 0.2;

    arrayFocos[0]->luzAmbiente[1] = 0.2;

    arrayFocos[0]->luzAmbiente[2] = 0.2;

    arrayFocos[0]->luzAmbiente[3] = 1.0;

    arrayFocos[0]->translX = 0.0;

    arrayFocos[0]->translY = 0.0;

    arrayFocos[0]->translZ = 0.0;

    arrayFocos[0]->angRotXX = 0.0;

    arrayFocos[0]->angRotYY = 0.0;

    arrayFocos[0]->angRotZZ = 0.0;

    arrayFocos[0]->rotacaoOnXX = 0;

    arrayFocos[0]->rotacaoOnYY = 0;

    arrayFocos[0]->rotacaoOnZZ = 0;
}

void inicializarModelos( void )
{
    int i, j;

    numModelos = 27;

    /* Criar o array */

    arrayModelos = (pontModelo*) malloc( numModelos * sizeof(pontModelo) );

    /* Carregar ficheiro de cores */

    cores = (GLfloat*) malloc( 6 * 3 * sizeof( GLfloat ) );

    lerCoresDeFicheiro( "cores.txt", &cores );

    arrCores = (GLfloat**) malloc(numModelos * sizeof(GLfloat*));

    /* Foco i */

    for (i = 0; i < numModelos; i++)
    {
        arrayModelos[i] = (pontModelo) malloc( sizeof(Registo_Modelo) );

        lerVerticesDeFicheiro( "modeloCubo.txt", &(arrayModelos[i]->numVertices),
                                                     &(arrayModelos[i]->arrayVertices) );

        /* Determinar as normais unitarias a cada triangulo */

        arrayModelos[i]->arrayNormais = calcularNormaisTriangulos( arrayModelos[i]->numVertices,
                                                                   arrayModelos[i]->arrayVertices );

        /* Array vazio para guardar a cor calculada para cada vertice */

        arrayModelos[i]->arrayCores = (GLfloat*) calloc( 3 * arrayModelos[i]->numVertices, sizeof( GLfloat) );

        arrCores[i] = (GLfloat*) calloc (3 * arrayModelos[i]->numVertices, sizeof( GLfloat));

        ind = i;

        /* Propriedades do material */

        arrayModelos[i]->kAmb[0] = 0.2;

        arrayModelos[i]->kAmb[1] = 0.2;

        arrayModelos[i]->kAmb[2] = 0.2;

        arrayModelos[i]->kAmb[3] = 1.0;

        arrayModelos[i]->kDif[0] = 0.2;

        arrayModelos[i]->kDif[1] = 0.2;

        arrayModelos[i]->kDif[2] = 0.2;

        arrayModelos[i]->kDif[3] = 1.0;

        arrayModelos[i]->kEsp[0] = 0.7;

        arrayModelos[i]->kEsp[1] = 0.7;

        arrayModelos[i]->kEsp[2] = 0.7;

        arrayModelos[i]->kEsp[3] = 1.0;

        arrayModelos[i]->coefDePhong = 100;

        /* Parametros das transformacoes */

        arrayModelos[i]->deslX = desl[i][0];

        arrayModelos[i]->deslY = desl[i][1];

        arrayModelos[i]->deslZ = desl[i][2];

        arrayModelos[i]->angRotXX = 0.0;

        arrayModelos[i]->angRotYY = 0.0;

        arrayModelos[i]->angRotZZ = 0.0;

        arrayModelos[i]->factorEscX = 1.5;

        arrayModelos[i]->factorEscY = 1.5;

        arrayModelos[i]->factorEscZ = 1.5;

        arrayModelos[i]->rotacaoOnXX = 0;

        arrayModelos[i]->rotacaoOnYY = 0;

        arrayModelos[i]->rotacaoOnZZ = 0;
    }
}

void inicializarRubik( void )
{
    int i, j, k;

    /* Inicializar a topologia do cubo. */

    rubik[0][0][0] = arrayModelos[0];
    rubik[1][0][0] = arrayModelos[1];
    rubik[2][0][0] = arrayModelos[2];
    rubik[0][1][0] = arrayModelos[3];
    rubik[1][1][0] = arrayModelos[4];
    rubik[2][1][0] = arrayModelos[5];
    rubik[0][2][0] = arrayModelos[6];
    rubik[1][2][0] = arrayModelos[7];
    rubik[2][2][0] = arrayModelos[8];
    rubik[0][0][1] = arrayModelos[9];
    rubik[1][0][1] = arrayModelos[10];
    rubik[2][0][1] = arrayModelos[11];
    rubik[0][1][1] = arrayModelos[12];
    rubik[1][1][1] = arrayModelos[13];
    rubik[2][1][1] = arrayModelos[14];
    rubik[0][2][1] = arrayModelos[15];
    rubik[1][2][1] = arrayModelos[16];
    rubik[2][2][1] = arrayModelos[17];
    rubik[0][0][2] = arrayModelos[18];
    rubik[1][0][2] = arrayModelos[19];
    rubik[2][0][2] = arrayModelos[20];
    rubik[0][1][2] = arrayModelos[21];
    rubik[1][1][2] = arrayModelos[22];
    rubik[2][1][2] = arrayModelos[23];
    rubik[0][2][2] = arrayModelos[24];
    rubik[1][2][2] = arrayModelos[25];
    rubik[2][2][2] = arrayModelos[26];

    associarCoresACubo(cores);

    /* Eixo de rota��o inicial. */

    axis = ZZ_AXIS;

    /* Fila de rota��o inicial. */

    num = 2;

    /* Debug */

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            rubik[i][j][num]->kDif[0] = 0.5;

            rubik[i][j][num]->kDif[1] = 0.5;

            rubik[i][j][num]->kDif[2] = 0.5;
        }
    }

    /* Estado inicial do cubo. */

    state = RUBIK_IDLE;

    /* Matrizes de rota��o para as anima��es. */

    mRotX = IDENTITY_MATRIX;

    mRotX_ = IDENTITY_MATRIX;

    mRotY = IDENTITY_MATRIX;

    mRotY_ = IDENTITY_MATRIX;

    mRotZ = IDENTITY_MATRIX;

    mRotZ_ = IDENTITY_MATRIX;

    RotateAboutX ( &mRotX, DegreesToRadians(ANGLE_BY_FRAME) );

    RotateAboutX ( &mRotX_, DegreesToRadians(-ANGLE_BY_FRAME) );

    RotateAboutY ( &mRotY, DegreesToRadians(ANGLE_BY_FRAME) );

    RotateAboutY ( &mRotY_, DegreesToRadians(-ANGLE_BY_FRAME) );

    RotateAboutZ ( &mRotZ, DegreesToRadians(ANGLE_BY_FRAME) );

    RotateAboutZ ( &mRotZ_, DegreesToRadians(-ANGLE_BY_FRAME) );

    /* Flag */

    solved = 0;

    /* Angulos de visualiza��o */

    visAngleXX = 45.0;

    visAngleYY = 30.0;

    /* Pontua��o */

    numRots = 0;

    /* Shuffle */

    /* Contadors do n�mero de rota��es do shuffle */

    shuffle = 0;

    /* Inicializar os algoritmos de shuffle */

    shuffleArray = (shufflePointer*)malloc(NALGORITHMS * sizeof(shufflePointer));

    for(i = 0; i < NALGORITHMS; i++)
    {
        shuffleArray[i] = (shufflePointer)malloc(sizeof(shuffle_algorithm));

        if(i == 0)
        {
            shuffleArray[i]->axis = (GLfloat*)malloc(39 * sizeof(GLfloat));

            shuffleArray[i]->axis[0] = YY_AXIS;
            shuffleArray[i]->axis[1] = YY_AXIS;
            shuffleArray[i]->axis[2] = YY_AXIS;
            shuffleArray[i]->axis[3] = XX_AXIS;
            shuffleArray[i]->axis[4] = XX_AXIS;
            shuffleArray[i]->axis[5] = ZZ_AXIS;
            shuffleArray[i]->axis[6] = ZZ_AXIS;
            shuffleArray[i]->axis[7] = ZZ_AXIS;
            shuffleArray[i]->axis[8] = ZZ_AXIS;
            shuffleArray[i]->axis[9] = ZZ_AXIS;
            shuffleArray[i]->axis[10] = YY_AXIS;
            shuffleArray[i]->axis[11] = YY_AXIS;
            shuffleArray[i]->axis[12] = YY_AXIS;
            shuffleArray[i]->axis[13] = YY_AXIS;
            shuffleArray[i]->axis[14] = XX_AXIS;
            shuffleArray[i]->axis[15] = XX_AXIS;
            shuffleArray[i]->axis[16] = XX_AXIS;
            shuffleArray[i]->axis[17] = YY_AXIS;
            shuffleArray[i]->axis[18] = YY_AXIS;
            shuffleArray[i]->axis[19] = YY_AXIS;
            shuffleArray[i]->axis[20] = XX_AXIS;
            shuffleArray[i]->axis[21] = YY_AXIS;
            shuffleArray[i]->axis[22] = ZZ_AXIS;
            shuffleArray[i]->axis[23] = ZZ_AXIS;
            shuffleArray[i]->axis[24] = YY_AXIS;
            shuffleArray[i]->axis[25] = YY_AXIS;
            shuffleArray[i]->axis[26] = YY_AXIS;
            shuffleArray[i]->axis[27] = YY_AXIS;
            shuffleArray[i]->axis[28] = YY_AXIS;
            shuffleArray[i]->axis[29] = YY_AXIS;
            shuffleArray[i]->axis[30] = XX_AXIS;
            shuffleArray[i]->axis[31] = XX_AXIS;
            shuffleArray[i]->axis[32] = XX_AXIS;
            shuffleArray[i]->axis[33] = XX_AXIS;
            shuffleArray[i]->axis[34] = ZZ_AXIS;
            shuffleArray[i]->axis[35] = ZZ_AXIS;
            shuffleArray[i]->axis[36] = ZZ_AXIS;
            shuffleArray[i]->axis[37] = ZZ_AXIS;
            shuffleArray[i]->axis[38] = ZZ_AXIS;

            shuffleArray[i]->num = (int*)malloc(39 * sizeof(int));

            shuffleArray[i]->num[0] = 0;
            shuffleArray[i]->num[1] = 2;
            shuffleArray[i]->num[2] = 2;
            shuffleArray[i]->num[3] = 0;
            shuffleArray[i]->num[4] = 0;
            shuffleArray[i]->num[5] = 2;
            shuffleArray[i]->num[6] = 2;
            shuffleArray[i]->num[7] = 0;
            shuffleArray[i]->num[8] = 0;
            shuffleArray[i]->num[9] = 0;
            shuffleArray[i]->num[10] = 0;
            shuffleArray[i]->num[11] = 0;
            shuffleArray[i]->num[12] = 2;
            shuffleArray[i]->num[13] = 2;
            shuffleArray[i]->num[14] = 0;
            shuffleArray[i]->num[15] = 0;
            shuffleArray[i]->num[16] = 2;
            shuffleArray[i]->num[17] = 2;
            shuffleArray[i]->num[18] = 2;
            shuffleArray[i]->num[19] = 2;
            shuffleArray[i]->num[20] = 2;
            shuffleArray[i]->num[21] = 2;
            shuffleArray[i]->num[22] = 2;
            shuffleArray[i]->num[23] = 0;
            shuffleArray[i]->num[24] = 0;
            shuffleArray[i]->num[25] = 0;
            shuffleArray[i]->num[26] = 0;
            shuffleArray[i]->num[27] = 2;
            shuffleArray[i]->num[28] = 2;
            shuffleArray[i]->num[29] = 2;
            shuffleArray[i]->num[30] = 0;
            shuffleArray[i]->num[31] = 2;
            shuffleArray[i]->num[32] = 2;
            shuffleArray[i]->num[33] = 2;
            shuffleArray[i]->num[34] = 2;
            shuffleArray[i]->num[35] = 2;
            shuffleArray[i]->num[36] = 0;
            shuffleArray[i]->num[37] = 0;
            shuffleArray[i]->num[38] = 0;

            shuffleArray[i]->nrots = 39;
        }
        else if(i == 1)
        {
            shuffleArray[i]->axis = (GLfloat*)malloc(32 * sizeof(GLfloat));

            shuffleArray[i]->axis[0] = XX_AXIS;
            shuffleArray[i]->axis[1] = YY_AXIS;
            shuffleArray[i]->axis[2] = YY_AXIS;
            shuffleArray[i]->axis[3] = XX_AXIS;
            shuffleArray[i]->axis[4] = YY_AXIS;
            shuffleArray[i]->axis[5] = ZZ_AXIS;
            shuffleArray[i]->axis[6] = XX_AXIS;
            shuffleArray[i]->axis[7] = XX_AXIS;
            shuffleArray[i]->axis[8] = XX_AXIS;
            shuffleArray[i]->axis[9] = XX_AXIS;
            shuffleArray[i]->axis[10] = YY_AXIS;
            shuffleArray[i]->axis[11] = YY_AXIS;
            shuffleArray[i]->axis[12] = ZZ_AXIS;
            shuffleArray[i]->axis[13] = ZZ_AXIS;
            shuffleArray[i]->axis[14] = XX_AXIS;
            shuffleArray[i]->axis[15] = ZZ_AXIS;
            shuffleArray[i]->axis[16] = ZZ_AXIS;
            shuffleArray[i]->axis[17] = ZZ_AXIS;
            shuffleArray[i]->axis[18] = ZZ_AXIS;
            shuffleArray[i]->axis[19] = ZZ_AXIS;
            shuffleArray[i]->axis[20] = YY_AXIS;
            shuffleArray[i]->axis[21] = ZZ_AXIS;
            shuffleArray[i]->axis[22] = ZZ_AXIS;
            shuffleArray[i]->axis[23] = ZZ_AXIS;
            shuffleArray[i]->axis[24] = ZZ_AXIS;
            shuffleArray[i]->axis[25] = XX_AXIS;
            shuffleArray[i]->axis[26] = XX_AXIS;
            shuffleArray[i]->axis[27] = XX_AXIS;
            shuffleArray[i]->axis[28] = XX_AXIS;
            shuffleArray[i]->axis[29] = YY_AXIS;
            shuffleArray[i]->axis[30] = YY_AXIS;
            shuffleArray[i]->axis[31] = YY_AXIS;

            shuffleArray[i]->num = (int*)malloc(32 * sizeof(int));

            shuffleArray[i]->num[0] = 0;
            shuffleArray[i]->num[1] = 0;
            shuffleArray[i]->num[2] = 2;
            shuffleArray[i]->num[3] = 0;
            shuffleArray[i]->num[4] = 0;
            shuffleArray[i]->num[5] = 0;
            shuffleArray[i]->num[6] = 0;
            shuffleArray[i]->num[7] = 0;
            shuffleArray[i]->num[8] = 2;
            shuffleArray[i]->num[9] = 2;
            shuffleArray[i]->num[10] = 0;
            shuffleArray[i]->num[11] = 0;
            shuffleArray[i]->num[12] = 0;
            shuffleArray[i]->num[13] = 0;
            shuffleArray[i]->num[14] = 0;
            shuffleArray[i]->num[15] = 0;
            shuffleArray[i]->num[16] = 0;
            shuffleArray[i]->num[17] = 0;
            shuffleArray[i]->num[18] = 0;
            shuffleArray[i]->num[19] = 0;
            shuffleArray[i]->num[20] = 0;
            shuffleArray[i]->num[21] = 0;
            shuffleArray[i]->num[22] = 2;
            shuffleArray[i]->num[23] = 2;
            shuffleArray[i]->num[24] = 2;
            shuffleArray[i]->num[25] = 0;
            shuffleArray[i]->num[26] = 0;
            shuffleArray[i]->num[27] = 0;
            shuffleArray[i]->num[28] = 2;
            shuffleArray[i]->num[29] = 0;
            shuffleArray[i]->num[30] = 2;
            shuffleArray[i]->num[31] = 2;

            shuffleArray[i]->nrots = 32;
        }
        else if(i == 2)
        {
            shuffleArray[i]->axis = (GLfloat*)malloc(34 * sizeof(GLfloat));

            shuffleArray[i]->axis[0] = YY_AXIS;
            shuffleArray[i]->axis[1] = YY_AXIS;
            shuffleArray[i]->axis[2] = YY_AXIS;
            shuffleArray[i]->axis[3] = XX_AXIS;
            shuffleArray[i]->axis[4] = ZZ_AXIS;
            shuffleArray[i]->axis[5] = ZZ_AXIS;
            shuffleArray[i]->axis[6] = ZZ_AXIS;
            shuffleArray[i]->axis[7] = YY_AXIS;
            shuffleArray[i]->axis[8] = ZZ_AXIS;
            shuffleArray[i]->axis[9] = ZZ_AXIS;
            shuffleArray[i]->axis[10] = XX_AXIS;
            shuffleArray[i]->axis[11] = XX_AXIS;
            shuffleArray[i]->axis[12] = XX_AXIS;
            shuffleArray[i]->axis[13] = XX_AXIS;
            shuffleArray[i]->axis[14] = ZZ_AXIS;
            shuffleArray[i]->axis[15] = ZZ_AXIS;
            shuffleArray[i]->axis[16] = YY_AXIS;
            shuffleArray[i]->axis[17] = ZZ_AXIS;
            shuffleArray[i]->axis[18] = ZZ_AXIS;
            shuffleArray[i]->axis[19] = YY_AXIS;
            shuffleArray[i]->axis[20] = YY_AXIS;
            shuffleArray[i]->axis[21] = YY_AXIS;
            shuffleArray[i]->axis[22] = ZZ_AXIS;
            shuffleArray[i]->axis[23] = ZZ_AXIS;
            shuffleArray[i]->axis[24] = YY_AXIS;
            shuffleArray[i]->axis[25] = XX_AXIS;
            shuffleArray[i]->axis[26] = XX_AXIS;
            shuffleArray[i]->axis[27] = ZZ_AXIS;
            shuffleArray[i]->axis[28] = ZZ_AXIS;
            shuffleArray[i]->axis[29] = YY_AXIS;
            shuffleArray[i]->axis[30] = YY_AXIS;
            shuffleArray[i]->axis[31] = XX_AXIS;
            shuffleArray[i]->axis[32] = XX_AXIS;
            shuffleArray[i]->axis[33] = XX_AXIS;

            shuffleArray[i]->num = (int*)malloc(34 * sizeof(int));

            shuffleArray[i]->num[0] = 2;
            shuffleArray[i]->num[1] = 2;
            shuffleArray[i]->num[2] = 2;
            shuffleArray[i]->num[3] = 2;
            shuffleArray[i]->num[4] = 2;
            shuffleArray[i]->num[5] = 2;
            shuffleArray[i]->num[6] = 2;
            shuffleArray[i]->num[7] = 2;
            shuffleArray[i]->num[8] = 0;
            shuffleArray[i]->num[9] = 0;
            shuffleArray[i]->num[10] = 0;
            shuffleArray[i]->num[11] = 0;
            shuffleArray[i]->num[12] = 2;
            shuffleArray[i]->num[13] = 2;
            shuffleArray[i]->num[14] = 0;
            shuffleArray[i]->num[15] = 0;
            shuffleArray[i]->num[16] = 2;
            shuffleArray[i]->num[17] = 0;
            shuffleArray[i]->num[18] = 2;
            shuffleArray[i]->num[19] = 2;
            shuffleArray[i]->num[20] = 2;
            shuffleArray[i]->num[21] = 0;
            shuffleArray[i]->num[22] = 0;
            shuffleArray[i]->num[23] = 0;
            shuffleArray[i]->num[24] = 0;
            shuffleArray[i]->num[25] = 0;
            shuffleArray[i]->num[26] = 2;
            shuffleArray[i]->num[27] = 2;
            shuffleArray[i]->num[28] = 0;
            shuffleArray[i]->num[29] = 2;
            shuffleArray[i]->num[30] = 2;
            shuffleArray[i]->num[31] = 2;
            shuffleArray[i]->num[32] = 2;
            shuffleArray[i]->num[33] = 2;

            shuffleArray[i]->nrots = 34;
        }
    }
}


void libertarArraysModelo( int i )
{
     free( arrayModelos[i]->arrayVertices );

     free( arrayModelos[i]->arrayNormais );

     free( arrayModelos[i]->arrayCores );
}


void libertarModelos( void )
{
    int i;

    for( i = 0; i < numModelos; i++ )
    {
        free( arrayModelos[i]->arrayVertices );

        free( arrayModelos[i]->arrayNormais );

        free( arrayModelos[i]->arrayCores );

        free( arrayModelos[i] );
    }

    free( arrayModelos );

    numModelos = 0;
}
