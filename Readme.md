4th Year Course - Visual Computing
-------------------------------------------

Project Members
---------------
Simão Reis 

Miguel Valério

Project Description
-------------------
Implementation of a Rubik Cube game.
Supports simple mouse control to change the cube perspective and keyboard controls to move the cube.
A reset button allows to shuffle the cube.

Implementation
--------------
OpenGL
