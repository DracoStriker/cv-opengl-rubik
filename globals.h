/*
 * globals.h
 *
 * Declaracao de tipos e variaveis globais.
 *
 * J. Madeira - Nov/2012
 * Sim�o Reis - Nov/2013
 * Miguel Val�rio - Nov/2013
 *
 */


#ifndef _globals_h
#define _globals_h


#define GLEW_STATIC /* Necessario se houver problemas com a lib */

#include <GL/glew.h>

#include <GL/freeglut.h>


#include "mathUtils.h"

#define NALGORITHMS 3

#define TRUE 1
#define FALSE 0

#define RUBIK_IDLE 0
#define RUBIK_ROTATION 1

#define XX_AXIS 0
#define YY_AXIS 1
#define ZZ_AXIS 2

#define CLOCKWISE 0
#define ANTICLOCKWISE 1

#define TOTAL_ANGLE 90
#define ANIMATION_TIME 20
#define ANGLE_BY_FRAME 10

/* Variaveis GLOBAIS !! */


int rotations[3][3][8];

int windowHandle;

/* Shuffle Algorithm */

typedef struct {
    int nrots;
    int *num;
    GLfloat *axis;
} shuffle_algorithm;

typedef shuffle_algorithm *shufflePointer;

shufflePointer* shuffleArray;

/* O identificador do programa em GLSL combinando os SHADERS */

GLuint programaGLSL;

/* Os identificadores dos SHADERS */

GLuint vs;

GLuint fs;

/* Para passar coordenadas, cores a a matriz de transformacao ao Vertex-Shader */

GLint attribute_coord3d;

GLint attribute_corRGB;

GLint uniform_matriz_proj;

GLint uniform_matriz_model_view;

/* Matriz de projeccao */

mat4x4 matrizProj;

/*** NOVO ***/

/* Para os MODELOS */

typedef struct {

    /* O modelo */

    /* Entidades */

    GLsizei numVertices;

    GLfloat* arrayVertices;

    GLfloat* arrayNormais;

    /* Cores para cada vertice */

    GLfloat* arrayCores;

    /* Propriedades */

    /* Arrays */

    GLfloat kAmb[4];

    GLfloat kDif[4];

    GLfloat kEsp[4];

    GLfloat coefDePhong;

    /* Parametros das transformacoes */

	float deslX, deslY, deslZ;

	float angRotXX, angRotYY, angRotZZ;

	float factorEscX, factorEscY, factorEscZ;

    /* Controlos de animacao */

    int rotacaoOnXX, rotacaoOnYY, rotacaoOnZZ;

} Registo_Modelo;

typedef Registo_Modelo *pontModelo;


/* O ARRAY de modelos deste exemplo */

pontModelo* arrayModelos;

int numModelos;

/* A topologia do array de rubik */

pontModelo rubik[3][3][3];

unsigned int axis;

unsigned int num;

int state;

int rubikSolved();

void rotateRubik(int);

int solved;

/* Rotation Matrices */

mat4x4 mRotX;

mat4x4 mRotX_;

mat4x4 mRotY;

mat4x4 mRotY_;

mat4x4 mRotZ;

mat4x4 mRotZ_;

/* Angulos para visualiza��o */

float visAngleXX;

float visAngleYY;

/* Cores */

GLfloat *cores;

/* Pontua��o */

int numRots;

/* Flag de shuffle */

int shuffle;

/* Para os FOCOS PONTUAIS */

/* Registo para guardar as caracteristicas e os parametros globais
   de transformacao de um foco                                      */

typedef struct {

    int focoIsOn;

    /* Posicao em Coordenada Homogeneas */

    GLfloat posicao[4];

    GLfloat intensidade[4];

    GLfloat luzAmbiente[4];

    /* Parametros das transformacoes */

	float translX, translY, translZ;

	float angRotXX, angRotYY, angRotZZ;

    /* Controlos de animacao */

    int rotacaoOnXX, rotacaoOnYY, rotacaoOnZZ;

} Registo_Foco;

typedef Registo_Foco *pontFoco;

/* O ARRAY de focos de luz deste exemplo */

pontFoco* arrayFocos;

int numFocos;

/* Matriz global de transformacao */

mat4x4 matrizModelView;

/* Parametros de transformacao da CENA */

float angRotXX, angRotYY, angRotZZ;

/* FLAGS para controlar a animacao */

GLboolean animacaoModelosON;

GLboolean animacaoFocosON;

GLboolean animacaoCenaON;

GLfloat rotX, rotY, orgX, orgY;

char setOrigin;

GLfloat **arrCores;

int ind;

#endif
