/*
 * callbacks.c
 *
 * J. Madeira - Nov/2012
 * Sim�o Reis - Nov/2013
 * Miguel Val�rio - Nov/2013
 *
 */


#include <stdlib.h>

#include <stdio.h>

#include <string.h>

#include <math.h>

#include <time.h>


#define GLEW_STATIC /* Necessario se houver problemas com a lib */

#include <GL/glew.h>

#include <GL/freeglut.h>


#include "globals.h"

#include "callbacks.h"

#include "consoleIO.h"

#include "shading.h"

/* Indices para os algoritmos de shuffle */

int index, randindex;

/* Matrix auxiliar para rota��es */

rotations[3][3][8] = {
    {
        {24,21,18, 9, 0, 3, 6,15},
        {25,22,19,10, 1, 4, 7,16},
        {26,23,20,11, 2, 5, 8,17}
    },
    {
        {18,19,20,11, 2, 1, 0, 9},
        {21,22,23,14, 5, 4, 3,12},
        {24,25,26,17, 8, 7, 6,15}
    },
    {
        { 0, 1, 2, 5, 8, 7, 6, 3},
        { 9,10,11,14,17,16,15,12},
        {18,19,20,23,26,25,24,21}
    },
};

/* Alus�o �s fun��es auxiliares */

static void debugLog( void );
static void swapColors(int, int, GLfloat**);
static void setColor(int, GLfloat**, GLfloat *);
static void getColor(int, GLfloat*, GLfloat **);
static void switchColors(int, int, int);
static void scrambleCube(void);

/* Callback functions */

void myDisplay( void )
{
    int m;

    /* Limpar a janela */

    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    /* SHADERS */

    glUseProgram( programaGLSL );

    /* Para cada um dos modelos da cena */

    for( m = 0; m < numModelos; m++ )
    {

        /* Input para o Vertex-Shader */

        glEnableVertexAttribArray( attribute_coord3d );

        glEnableVertexAttribArray( attribute_corRGB );

        /* Caracteristicas do array de coordenadas */

        glVertexAttribPointer( attribute_coord3d, // attribute

                          3,                 // number of elements per vertex, here (x,y,z)

                          GL_FLOAT,          // the type of each element

                          GL_FALSE,          // take our values as-is

                          0,                 // no extra data between each position

                          arrayModelos[m]->arrayVertices );   // pointer to the C array

        /* Caracteristicas do array de cores */

        glVertexAttribPointer( attribute_corRGB, // attribute

                          3,                 // number of elements per vertex, here (R,G,B)

                          GL_FLOAT,          // the type of each element

                          GL_FALSE,          // take our values as-is

                          0,                 // no extra data between each position

                          arrayModelos[m]->arrayCores );    // pointer to the C array

        /* ATENCAO : Ordem das transformacoes !! */

        matrizModelView = IDENTITY_MATRIX;

        /* Transforma��es globais para a muda�a de perspectiva de visualiza��o */

        Translate( &matrizModelView, 0.0, 0.0, -8.0 );

        RotateAboutX( &matrizModelView, DegreesToRadians( visAngleXX ) );

        RotateAboutY( &matrizModelView, DegreesToRadians( visAngleYY ) );

        /* Transformacoes proprias do m-esimo modelo */

        Translate( &matrizModelView, arrayModelos[m]->deslX, arrayModelos[m]->deslY, arrayModelos[m]->deslZ );

        RotateAboutX( &matrizModelView, DegreesToRadians( arrayModelos[m]->angRotXX ) );

        RotateAboutY( &matrizModelView, DegreesToRadians( arrayModelos[m]->angRotYY ) );

        RotateAboutZ( &matrizModelView, DegreesToRadians( arrayModelos[m]->angRotZZ ) );

        Scale( &matrizModelView, arrayModelos[m]->factorEscX, arrayModelos[m]->factorEscY, arrayModelos[m]->factorEscZ );

        /* Matriz de projeccao */

        glUniformMatrix4fv( uniform_matriz_proj, 1, GL_FALSE, matrizProj.m );

        /* Matriz de transformacao */

        glUniformMatrix4fv( uniform_matriz_model_view, 1, GL_FALSE, matrizModelView.m );

        /* Aplicar o Modelo de Ilumina��o ao m-esimo modelo */

        smoothShading( m );

        glDrawArrays( GL_TRIANGLES, 0, arrayModelos[m]->numVertices );

        glDisableVertexAttribArray( attribute_coord3d );

        glDisableVertexAttribArray( attribute_corRGB );

    }

    /* Display the result */

    /* DOUBLE-BUFFERING */

    glutSwapBuffers();
}


void myKeyboard( unsigned char key, int x, int y )
{

    switch( key )
    {
        /* Mudar a fatia selecionada do rubik para o eixo XX */

        case 'z':

        case 'Z':

            if ( state == RUBIK_IDLE )
            {
                axis = XX_AXIS;

                debugLog();

                glutPostRedisplay();
            }

            break;

        /* Mudar a fatia selecionada do rubik para o eixo YY */

        case 'x':

        case 'X':

            if ( state == RUBIK_IDLE )
            {
                axis = YY_AXIS;

                debugLog();

                glutPostRedisplay();
            }

            break;

        /* Mudar a fatia selecionada do rubik para o eixo ZZ */

        case 'c':

        case 'C':

            if ( state == RUBIK_IDLE )
            {
                axis = ZZ_AXIS;

                debugLog();

                glutPostRedisplay();
            }

            break;

        case 'q':

        case 'Q':

        case 27:

            exit( EXIT_SUCCESS );

            break;

        /* Baralhar o cubo. */

        case 's':

        case 'S':

            if ( (state == RUBIK_IDLE) /* && (solved == FALSE) */ )
            {
                index = 0;

                state = RUBIK_ROTATION;

                rotateRubik( ANTICLOCKWISE );

                debugLog();

                srand(time(NULL));

                randindex = rand() % NALGORITHMS;

                shuffle = shuffleArray[randindex]->nrots;

                glutTimerFunc( 0, rotateANTICLOCKWISE, 0 );
            }

            break;
    }
}


void mySpecialKeys( int key, int x, int y )
{
	/* Usar as teclas de cursor para controlar as rotacoes */

    switch( key )
    {
        /* Mudar para a fila anterior no eixo selecionado */

        case GLUT_KEY_DOWN :

            if ( ( num > 0 ) && ( state == RUBIK_IDLE ) )
            {
                num--;

                debugLog();

                glutPostRedisplay();
            }

            break;

        /* Mudar para a fila seguinte no eixo selecionado */

        case GLUT_KEY_UP :

            if ( ( num < 2 ) && ( state == RUBIK_IDLE ) )
            {
                num++;

                debugLog();

                glutPostRedisplay();
            }

            break;

        /* Iniciar uma rota��o em sentido hor�rio */

        case GLUT_KEY_LEFT :

            if ( (state == RUBIK_IDLE) /* && (solved == FALSE) */ )
            {
                state = RUBIK_ROTATION;

                rotateRubik( CLOCKWISE );

                debugLog();

                numRots++;

                glutTimerFunc( 0, rotateCLOCKWISE, 0 );
            }

            break;

        /* Iniciar uma rota��o em sentido anti hor�rio */

        case GLUT_KEY_RIGHT :

            if ( (state == RUBIK_IDLE) /* && (solved == FALSE) */ )
            {
                state = RUBIK_ROTATION;

                rotateRubik( ANTICLOCKWISE );

                debugLog();

                numRots++;

                glutTimerFunc( 0, rotateANTICLOCKWISE, 0 );
            }

            break;
    }
}

/* Mudar a perspectiva de visualiza��o do Rubik */

void Mouse(int x, int y)
{
    if(setOrigin)
    {
        orgX = x;

        orgY = y;

        setOrigin = 0;
    }

    else
    {
        if( abs( visAngleXX ) >= 360 )
        {
            visAngleXX = 0;
        }

        rotX = y - orgY;

        rotY = x - orgX;

        if( (abs( visAngleXX ) >= 135) && (abs( visAngleXX ) < 315) )
        {
            visAngleYY -= rotY;

            visAngleXX += rotX;
        }

        else
        {
            visAngleYY += rotY;

            visAngleXX += rotX;
        }

        glutPostRedisplay();

        setOrigin = 1;
    }
}

void noMouse(int x, int y)
{
    setOrigin = 1;
}

void registarCallbackFunctions( void )
{
   glutDisplayFunc( myDisplay );

   glutKeyboardFunc( myKeyboard );

   glutSpecialFunc( mySpecialKeys );

   glutMotionFunc( Mouse );

   glutPassiveMotionFunc( noMouse );
}


/* Anima��o da rota��o em sentido anti hor�rio */

void rotateANTICLOCKWISE( int value )
{
    static int angle = 0;

    float vin[4];

    float *vout;

    GLfloat *tempColor;

    int i, j, k, l;

    vin[3] = 1;

    angle += ANGLE_BY_FRAME;

    if (angle <= TOTAL_ANGLE)
    {
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 3; j++)
            {
                switch( axis )
                {
                    case XX_AXIS:

                        rubik[num][i][j]->angRotXX += ANGLE_BY_FRAME;

                        if( rubik[num][i][j]->angRotXX >= 360.0 )
                        {
                            rubik[num][i][j]->angRotXX -= 360.0;
                        }

                        vin[0] = rubik[num][i][j]->deslX;

                        vin[1] = rubik[num][i][j]->deslY;

                        vin[2] = rubik[num][i][j]->deslZ;

                        vout = multiplyVectorByMatrix( &mRotX, vin );

                        rubik[num][i][j]->deslX = vout[0];

                        rubik[num][i][j]->deslY = vout[1];

                        rubik[num][i][j]->deslZ = vout[2];

                        break;

                    case YY_AXIS:

                        rubik[i][num][j]->angRotYY += ANGLE_BY_FRAME;

                        if( rubik[i][num][j]->angRotYY >= 360.0 )
                        {
                            rubik[i][num][j]->angRotYY -= 360.0;
                        }

                        vin[0] = rubik[i][num][j]->deslX;

                        vin[1] = rubik[i][num][j]->deslY;

                        vin[2] = rubik[i][num][j]->deslZ;

                        vout = multiplyVectorByMatrix( &mRotY, vin );

                        rubik[i][num][j]->deslX = vout[0];

                        rubik[i][num][j]->deslY = vout[1];

                        rubik[i][num][j]->deslZ = vout[2];

                        break;

                    case ZZ_AXIS:

                        rubik[i][j][num]->angRotZZ += ANGLE_BY_FRAME;

                        if( rubik[i][j][num]->angRotZZ >= 360.0 )
                        {
                            rubik[i][j][num]->angRotZZ -= 360.0;
                        }

                        vin[0] = rubik[i][j][num]->deslX;

                        vin[1] = rubik[i][j][num]->deslY;

                        vin[2] = rubik[i][j][num]->deslZ;

                        vout = multiplyVectorByMatrix( &mRotZ, vin );

                        rubik[i][j][num]->deslX = vout[0];

                        rubik[i][j][num]->deslY = vout[1];

                        rubik[i][j][num]->deslZ = vout[2];

                        break;
                }
            }
        }

        glutPostRedisplay();

        glutTimerFunc( ANIMATION_TIME, rotateANTICLOCKWISE, 0 );
    }

    else
    {
        /* Acertar as Cores!! */

        tempColor = (GLfloat*) malloc(18*sizeof(GLfloat));

        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 3; j++)
            {
                for(k = 0; k < 3; k++)
                {
                    rubik[i][j][k]->angRotXX = 0.0;
                    rubik[i][j][k]->angRotYY = 0.0;
                    rubik[i][j][k]->angRotZZ = 0.0;
                }
            }
        }

        switch( axis )
        {
            case XX_AXIS:

                for(i = 0; i < 8; i++)
                {
                    //Trocar as cores de cada um dos cubos pequenos, de acordo com a rota��o realizada

                    setColor(4, &tempColor, arrCores[rotations[axis][num][i]]);

                    swapColors(2, 4, &(arrCores[rotations[axis][num][i]]));

                    swapColors(5, 2, &(arrCores[rotations[axis][num][i]]));

                    swapColors(0, 5, &(arrCores[rotations[axis][num][i]]));

                    getColor(0, tempColor, &(arrCores[rotations[axis][num][i]]));
                }

                //Trocar os �ndices dos cubos referenciados pelo array rotations

                switchColors(0, XX_AXIS, num);

                break;

            case YY_AXIS:

                for(i = 0; i < 8; i++)
                {
                    //Trocar as cores de cada um dos cubos pequenos, de acordo com a rota��o realizada

                    setColor(0, &tempColor, arrCores[rotations[axis][num][i]]);

                    swapColors(3, 0, &(arrCores[rotations[axis][num][i]]));

                    swapColors(2, 3, &(arrCores[rotations[axis][num][i]]));

                    swapColors(1, 2, &(arrCores[rotations[axis][num][i]]));

                    getColor(1, tempColor, &(arrCores[rotations[axis][num][i]]));
                }

                //Trocar os �ndices dos cubos referenciados pelo array rotations

                switchColors(0, YY_AXIS, num);

                break;

            case ZZ_AXIS:

                for(i = 0; i < 8; i++)
                {
                    //Trocar as cores de cada um dos cubos pequenos, de acordo com a rota��o realizada

                    setColor(4, &tempColor, arrCores[rotations[axis][num][i]]);

                    swapColors(1, 4, &(arrCores[rotations[axis][num][i]]));

                    swapColors(5, 1, &(arrCores[rotations[axis][num][i]]));

                    swapColors(3, 5, &(arrCores[rotations[axis][num][i]]));

                    getColor(3, tempColor, &(arrCores[rotations[axis][num][i]]));
                }

                //Trocar os �ndices dos cubos referenciados pelo array rotations

                switchColors(0, ZZ_AXIS, num);

                break;
        }

        angle = 0;

        /* Shuffle */

        if ( shuffle )
        {
            if( (shuffle % 5) == 0 )
            {
                srand(time(NULL));

                num = rand() % 3;

                axis = rand() % 3;
            }

            else
            {
                num = shuffleArray[randindex]->num[index];

                axis = shuffleArray[randindex]->axis[index];
            }

            rotateRubik( ANTICLOCKWISE );

            debugLog();

            shuffle--;

            index++;

            glutTimerFunc( 0, rotateANTICLOCKWISE, 0 );
        }

        /* Terminar a rota��o! */

        else
        {
            state = RUBIK_IDLE;

            debugLog();

            solved = rubikSolved();

            if ( solved )
            {
                printf( "Rubik Solved!\nNumber of Rotations: %d \n\n", numRots );

                numRots = 0;
            }

            glutPostRedisplay();
        }
    }
}


/* Anima��o da rota��o em sentido hor�rio */

void rotateCLOCKWISE( int value )
{
    static int angle = 0;

    float vin[4];

    float *vout;

    GLfloat *tempColor;

    int i, j, k, l;

    vin[3] = 1;

    angle += ANGLE_BY_FRAME;

    if (angle <= TOTAL_ANGLE)
    {
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 3; j++)
            {
                switch( axis )
                {
                    case XX_AXIS:

                        rubik[num][i][j]->angRotXX -= ANGLE_BY_FRAME;

                        if( rubik[num][i][j]->angRotXX <= 0.0 )
                        {
                            rubik[num][i][j]->angRotXX += 360.0;
                        }

                        vin[0] = rubik[num][i][j]->deslX;

                        vin[1] = rubik[num][i][j]->deslY;

                        vin[2] = rubik[num][i][j]->deslZ;

                        vout = multiplyVectorByMatrix( &mRotX_, vin );

                        rubik[num][i][j]->deslX = vout[0];

                        rubik[num][i][j]->deslY = vout[1];

                        rubik[num][i][j]->deslZ = vout[2];

                        break;

                    case YY_AXIS:

                        rubik[i][num][j]->angRotYY -= ANGLE_BY_FRAME;

                        if( rubik[i][num][j]->angRotYY <= 0.0 )
                        {
                            rubik[i][num][j]->angRotYY += 360.0;
                        }

                        vin[0] = rubik[i][num][j]->deslX;

                        vin[1] = rubik[i][num][j]->deslY;

                        vin[2] = rubik[i][num][j]->deslZ;

                        vout = multiplyVectorByMatrix( &mRotY_, vin );

                        rubik[i][num][j]->deslX = vout[0];

                        rubik[i][num][j]->deslY = vout[1];

                        rubik[i][num][j]->deslZ = vout[2];

                        break;

                    case ZZ_AXIS:

                        rubik[i][j][num]->angRotZZ -= ANGLE_BY_FRAME;

                        if( rubik[i][j][num]->angRotZZ <= 0.0 )
                        {
                            rubik[i][j][num]->angRotZZ += 360.0;
                        }

                        vin[0] = rubik[i][j][num]->deslX;

                        vin[1] = rubik[i][j][num]->deslY;

                        vin[2] = rubik[i][j][num]->deslZ;

                        vout = multiplyVectorByMatrix( &mRotZ_, vin );

                        rubik[i][j][num]->deslX = vout[0];

                        rubik[i][j][num]->deslY = vout[1];

                        rubik[i][j][num]->deslZ = vout[2];

                        break;
                }
            }
        }

        glutPostRedisplay();

        glutTimerFunc( ANIMATION_TIME, rotateCLOCKWISE, 0 );
    }

    else
    {
        /* Acertar as Cores!! */

        tempColor = (GLfloat*) malloc(18*sizeof(GLfloat));

        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 3; j++)
            {
                for(k = 0; k < 3; k++)
                {
                    rubik[i][j][k]->angRotXX = 0.0;

                    rubik[i][j][k]->angRotYY = 0.0;

                    rubik[i][j][k]->angRotZZ = 0.0;
                }
            }
        }

        switch( axis )
        {
            case XX_AXIS:
            {
                for(i = 0; i < 8; i++)
                {
                    //Trocar as cores de cada um dos cubos pequenos, de acordo com a rota��o realizada

                    setColor(4, &tempColor, arrCores[rotations[axis][num][i]]);

                    swapColors(0, 4, &(arrCores[rotations[axis][num][i]]));

                    swapColors(5, 0, &(arrCores[rotations[axis][num][i]]));

                    swapColors(2, 5, &(arrCores[rotations[axis][num][i]]));

                    getColor(2, tempColor, &(arrCores[rotations[axis][num][i]]));
                }

                //Trocar os �ndices dos cubos referenciados pelo array rotations

                switchColors(1, XX_AXIS, num);

                break;
            }
            case YY_AXIS:

                for(i = 0; i < 8; i++)
                {
                    //Trocar as cores de cada um dos cubos pequenos, de acordo com a rota��o realizada

                    setColor(0, &tempColor, arrCores[rotations[axis][num][i]]);

                    swapColors(1, 0, &(arrCores[rotations[axis][num][i]]));

                    swapColors(2, 1, &(arrCores[rotations[axis][num][i]]));

                    swapColors(3, 2, &(arrCores[rotations[axis][num][i]]));

                    getColor(3, tempColor, &(arrCores[rotations[axis][num][i]]));
                }

                //Trocar os �ndices dos cubos referenciados pelo array rotations

                switchColors(1, YY_AXIS, num);

                break;

            case ZZ_AXIS:

                for(i = 0; i < 8; i++)
                {
                    //Trocar as cores de cada um dos cubos pequenos, de acordo com a rota��o realizada

                    setColor(4, &tempColor, arrCores[rotations[axis][num][i]]);

                    swapColors(3, 4, &(arrCores[rotations[axis][num][i]]));

                    swapColors(5, 3, &(arrCores[rotations[axis][num][i]]));

                    swapColors(1, 5, &(arrCores[rotations[axis][num][i]]));

                    getColor(1, tempColor, &(arrCores[rotations[axis][num][i]]));
                }

                //Trocar os �ndices dos cubos referenciados pelo array rotations

                switchColors(1, ZZ_AXIS, num);

                break;

        }

        angle = 0;

        /* Shuffle */

        if ( shuffle )
        {
            if( (shuffle % 5) == 0 )
            {
                srand(time(NULL));

                num = rand() % 3;

                axis = rand() % 3;
            }

            else
            {
                num = shuffleArray[randindex]->num[index];

                axis = shuffleArray[randindex]->axis[index];
            }

            rotateRubik( CLOCKWISE );

            debugLog();

            shuffle--;

            index++;

            glutTimerFunc( 0, rotateCLOCKWISE, 0 );
        }

        /* Terminar a rota��o! */

        else
        {
            state = RUBIK_IDLE;

            debugLog();

            solved = rubikSolved();

            if ( solved )
            {
                printf( "Rubik Solved!\nNumber of Rotations: %d \n\n", numRots );

                numRots = 0;
            }

            glutPostRedisplay();
        }
    }
}

/* Fun��es auxiliares */

/* Fun��o de debug e feedback para visualizar a fatia selecionada */

static void debugLog( void )
{
    int i, j;

    float c = 1.0;

    for (i = 0; i < 27; i++)
    {
        arrayModelos[i]->kDif[0] = 0.2;

        arrayModelos[i]->kDif[1] = 0.2;

        arrayModelos[i]->kDif[2] = 0.2;
    }

    switch ( axis )
    {
        case XX_AXIS :

            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    rubik[num][i][j]->kDif[0] = 0.5;

                    rubik[num][i][j]->kDif[1] = 0.5;

                    rubik[num][i][j]->kDif[2] = 0.5;
                }
            }

            break;

        case YY_AXIS :

            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    rubik[i][num][j]->kDif[0] = 0.5;

                    rubik[i][num][j]->kDif[1] = 0.5;

                    rubik[i][num][j]->kDif[2] = 0.5;
                }
            }

            break;

        case ZZ_AXIS :

            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    rubik[i][j][num]->kDif[0] = 0.5;

                    rubik[i][j][num]->kDif[1] = 0.5;

                    rubik[i][j][num]->kDif[2] = 0.5;
                }
            }

            break;
    }
}

static void swapColors(int index1, int index2, GLfloat** arr)
{
    int i, j;

    for(i = index1*18, j = index2*18; i < (index1*18)+18; i++, j++)
    {
        (*arr)[j] = (*arr)[i];
    }
}

static void setColor(int index, GLfloat **tempColor, GLfloat *arrayColor)
{
    int i, j;

    for(i = index*18, j = 0; i < (index+1)*18; i++, j++)
    {
        (*tempColor)[j] = arrayColor[i];
    }
}

static void getColor(int index, GLfloat *tempColor, GLfloat **arrayColor)
{
    int i, j;

    for(i = index*18, j = 0; i < (index+1)*18; i++, j++)
    {
        (*arrayColor)[i] = tempColor[j];
    }
}

static void switchColors(int rotation, int axis, int num)
{
    int i;

    int temp1, temp2;

    if(!rotation)
    {
        temp1 = rotations[axis][num][0];

        temp2 = rotations[axis][num][1];

        for(i = 7; i > 1; i--)
        {
            rotations[axis][num][(i+2)%8] = rotations[axis][num][i];
        }

        rotations[axis][num][3] = temp2;

        rotations[axis][num][2] = temp1;
    }

    else
    {
        temp1 = rotations[axis][num][7];

        temp2 = rotations[axis][num][6];

        for(i = 0; i < 6; i++)
        {
            if(i == 0)
            {
                rotations[axis][num][6] = rotations[axis][num][0];
            }

            else if(i == 1)
            {
                rotations[axis][num][7] = rotations[axis][num][1];
            }

            else
            {
                rotations[axis][num][i-2] = rotations[axis][num][i];
            }
        }

        rotations[axis][num][4] = temp2;

        rotations[axis][num][5] = temp1;
    }

    if(axis == XX_AXIS)
    {
        if(num == 2)
        {
            rotations[YY_AXIS][2][2] = rotations[axis][num][0];
            rotations[YY_AXIS][2][3] = rotations[axis][num][7];
            rotations[YY_AXIS][2][4] = rotations[axis][num][6];
            rotations[YY_AXIS][1][2] = rotations[axis][num][1];
            rotations[YY_AXIS][1][4] = rotations[axis][num][5];
            rotations[YY_AXIS][0][2] = rotations[axis][num][2];
            rotations[YY_AXIS][0][3] = rotations[axis][num][3];
            rotations[YY_AXIS][0][4] = rotations[axis][num][4];
            rotations[ZZ_AXIS][2][2] = rotations[axis][num][2];
            rotations[ZZ_AXIS][2][3] = rotations[axis][num][1];
            rotations[ZZ_AXIS][2][4] = rotations[axis][num][0];
            rotations[ZZ_AXIS][1][2] = rotations[axis][num][3];
            rotations[ZZ_AXIS][1][4] = rotations[axis][num][7];
            rotations[ZZ_AXIS][0][2] = rotations[axis][num][4];
            rotations[ZZ_AXIS][0][3] = rotations[axis][num][5];
            rotations[ZZ_AXIS][0][4] = rotations[axis][num][6];
        }
        else if(num == 1)
        {
            rotations[YY_AXIS][2][1] = rotations[axis][num][0];
            rotations[YY_AXIS][2][5] = rotations[axis][num][6];
            rotations[YY_AXIS][1][1] = rotations[axis][num][1];
            rotations[YY_AXIS][1][5] = rotations[axis][num][5];
            rotations[YY_AXIS][0][1] = rotations[axis][num][2];
            rotations[YY_AXIS][0][5] = rotations[axis][num][4];
            rotations[ZZ_AXIS][2][1] = rotations[axis][num][2];
            rotations[ZZ_AXIS][2][5] = rotations[axis][num][0];
            rotations[ZZ_AXIS][1][1] = rotations[axis][num][3];
            rotations[ZZ_AXIS][1][5] = rotations[axis][num][7];
            rotations[ZZ_AXIS][0][1] = rotations[axis][num][4];
            rotations[ZZ_AXIS][0][5] = rotations[axis][num][6];
        }
        else if(num == 0)
        {
            rotations[YY_AXIS][2][0] = rotations[axis][num][0];
            rotations[YY_AXIS][2][6] = rotations[axis][num][6];
            rotations[YY_AXIS][2][7] = rotations[axis][num][7];
            rotations[YY_AXIS][1][0] = rotations[axis][num][1];
            rotations[YY_AXIS][1][6] = rotations[axis][num][5];
            rotations[YY_AXIS][0][0] = rotations[axis][num][2];
            rotations[YY_AXIS][0][6] = rotations[axis][num][4];
            rotations[YY_AXIS][0][7] = rotations[axis][num][3];
            rotations[ZZ_AXIS][2][0] = rotations[axis][num][2];
            rotations[ZZ_AXIS][2][6] = rotations[axis][num][0];
            rotations[ZZ_AXIS][2][7] = rotations[axis][num][1];
            rotations[ZZ_AXIS][1][0] = rotations[axis][num][3];
            rotations[ZZ_AXIS][1][6] = rotations[axis][num][7];
            rotations[ZZ_AXIS][0][0] = rotations[axis][num][4];
            rotations[ZZ_AXIS][0][6] = rotations[axis][num][6];
            rotations[ZZ_AXIS][0][7] = rotations[axis][num][5];
        }
    }
    else if(axis == YY_AXIS)
    {
        if(num == 2)
        {
            rotations[XX_AXIS][2][0] = rotations[axis][num][2];
            rotations[XX_AXIS][2][6] = rotations[axis][num][4];
            rotations[XX_AXIS][2][7] = rotations[axis][num][3];
            rotations[XX_AXIS][1][0] = rotations[axis][num][1];
            rotations[XX_AXIS][1][6] = rotations[axis][num][5];
            rotations[XX_AXIS][0][0] = rotations[axis][num][0];
            rotations[XX_AXIS][0][6] = rotations[axis][num][6];
            rotations[XX_AXIS][0][7] = rotations[axis][num][7];
            rotations[ZZ_AXIS][2][4] = rotations[axis][num][2];
            rotations[ZZ_AXIS][2][5] = rotations[axis][num][1];
            rotations[ZZ_AXIS][2][6] = rotations[axis][num][0];
            rotations[ZZ_AXIS][1][4] = rotations[axis][num][3];
            rotations[ZZ_AXIS][1][6] = rotations[axis][num][7];
            rotations[ZZ_AXIS][0][4] = rotations[axis][num][4];
            rotations[ZZ_AXIS][0][5] = rotations[axis][num][5];
            rotations[ZZ_AXIS][0][6] = rotations[axis][num][6];
        }
        else if(num == 1)
        {
            rotations[XX_AXIS][2][1] = rotations[axis][num][2];
            rotations[XX_AXIS][2][5] = rotations[axis][num][4];
            rotations[XX_AXIS][1][1] = rotations[axis][num][1];
            rotations[XX_AXIS][1][5] = rotations[axis][num][5];
            rotations[XX_AXIS][0][1] = rotations[axis][num][0];
            rotations[XX_AXIS][0][5] = rotations[axis][num][6];
            rotations[ZZ_AXIS][2][3] = rotations[axis][num][2];
            rotations[ZZ_AXIS][2][7] = rotations[axis][num][0];
            rotations[ZZ_AXIS][1][3] = rotations[axis][num][3];
            rotations[ZZ_AXIS][1][7] = rotations[axis][num][7];
            rotations[ZZ_AXIS][0][3] = rotations[axis][num][4];
            rotations[ZZ_AXIS][0][7] = rotations[axis][num][6];
        }
        else if(num == 0)
        {
            rotations[XX_AXIS][2][2] = rotations[axis][num][2];
            rotations[XX_AXIS][2][3] = rotations[axis][num][3];
            rotations[XX_AXIS][2][4] = rotations[axis][num][4];
            rotations[XX_AXIS][1][2] = rotations[axis][num][1];
            rotations[XX_AXIS][1][4] = rotations[axis][num][5];
            rotations[XX_AXIS][0][2] = rotations[axis][num][0];
            rotations[XX_AXIS][0][3] = rotations[axis][num][7];
            rotations[XX_AXIS][0][4] = rotations[axis][num][6];
            rotations[ZZ_AXIS][2][0] = rotations[axis][num][0];
            rotations[ZZ_AXIS][2][1] = rotations[axis][num][1];
            rotations[ZZ_AXIS][2][2] = rotations[axis][num][2];
            rotations[ZZ_AXIS][1][0] = rotations[axis][num][7];
            rotations[ZZ_AXIS][1][2] = rotations[axis][num][3];
            rotations[ZZ_AXIS][0][0] = rotations[axis][num][6];
            rotations[ZZ_AXIS][0][1] = rotations[axis][num][5];
            rotations[ZZ_AXIS][0][2] = rotations[axis][num][4];
        }
    }
    else if(axis == ZZ_AXIS)
    {
        if(num == 2)
        {
            rotations[XX_AXIS][2][0] = rotations[axis][num][4];
            rotations[XX_AXIS][2][1] = rotations[axis][num][3];
            rotations[XX_AXIS][2][2] = rotations[axis][num][2];
            rotations[XX_AXIS][1][0] = rotations[axis][num][5];
            rotations[XX_AXIS][1][2] = rotations[axis][num][1];
            rotations[XX_AXIS][0][0] = rotations[axis][num][6];
            rotations[XX_AXIS][0][1] = rotations[axis][num][7];
            rotations[XX_AXIS][0][2] = rotations[axis][num][0];
            rotations[YY_AXIS][2][0] = rotations[axis][num][6];
            rotations[YY_AXIS][2][1] = rotations[axis][num][5];
            rotations[YY_AXIS][2][2] = rotations[axis][num][4];
            rotations[YY_AXIS][1][0] = rotations[axis][num][7];
            rotations[YY_AXIS][1][2] = rotations[axis][num][3];
            rotations[YY_AXIS][0][0] = rotations[axis][num][0];
            rotations[YY_AXIS][0][1] = rotations[axis][num][1];
            rotations[YY_AXIS][0][2] = rotations[axis][num][2];
        }
        else if(num == 1)
        {
            rotations[XX_AXIS][2][3] = rotations[axis][num][2];//
            rotations[XX_AXIS][2][7] = rotations[axis][num][4];//
            rotations[XX_AXIS][1][3] = rotations[axis][num][1];//
            rotations[XX_AXIS][1][7] = rotations[axis][num][5];//
            rotations[XX_AXIS][0][3] = rotations[axis][num][0];
            rotations[XX_AXIS][0][7] = rotations[axis][num][6];
            rotations[YY_AXIS][2][3] = rotations[axis][num][4];
            rotations[YY_AXIS][2][7] = rotations[axis][num][6];
            rotations[YY_AXIS][1][3] = rotations[axis][num][3];
            rotations[YY_AXIS][1][7] = rotations[axis][num][7];
            rotations[YY_AXIS][0][3] = rotations[axis][num][2];
            rotations[YY_AXIS][0][7] = rotations[axis][num][0];
        }
        else if(num == 0)
        {
            rotations[XX_AXIS][2][4] = rotations[axis][num][2];
            rotations[XX_AXIS][2][5] = rotations[axis][num][3];
            rotations[XX_AXIS][2][6] = rotations[axis][num][4];
            rotations[XX_AXIS][1][4] = rotations[axis][num][1];
            rotations[XX_AXIS][1][6] = rotations[axis][num][5];
            rotations[XX_AXIS][0][4] = rotations[axis][num][0];
            rotations[XX_AXIS][0][5] = rotations[axis][num][7];
            rotations[XX_AXIS][0][6] = rotations[axis][num][6];
            rotations[YY_AXIS][2][4] = rotations[axis][num][4];
            rotations[YY_AXIS][2][5] = rotations[axis][num][5];
            rotations[YY_AXIS][2][6] = rotations[axis][num][6];
            rotations[YY_AXIS][1][4] = rotations[axis][num][3];
            rotations[YY_AXIS][1][6] = rotations[axis][num][7];
            rotations[YY_AXIS][0][4] = rotations[axis][num][2];
            rotations[YY_AXIS][0][5] = rotations[axis][num][1];
            rotations[YY_AXIS][0][6] = rotations[axis][num][0];
        }
    }
}
