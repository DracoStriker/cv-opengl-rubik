/*
 * models.h
 *
 * Ficheiro cabecalho do modulo MODELS.
 *
 * J. Madeira - Out/2012
 */


#ifndef _models_h
#define _models_h


#define GLEW_STATIC /* Necessario se houver problemas com a lib */

#include <GL/glew.h>

#include <GL/freeglut.h>

void lerVerticesDeFicheiro( char* nome, int* numVertices, GLfloat** arrayVertices );

void escreverVerticesEmFicheiro( char* nome, int numVertices, GLfloat* arrayVertices );

void lerFicheiroOBJ( char* nome, int* numVertices, GLfloat** arrayVertices, GLfloat** arrayNormais );

void lerCoresDeFicheiro(char* nome, GLfloat** cores);

void associarCoresACubo(GLfloat* cores);

void escreverCores(char* nome, GLfloat* arr);

/* ------------------------- */

GLfloat* calcularNormaisTriangulos( int numVertices, GLfloat* arrayVertices );


static char faces[27][6] = {{0,0,0,1,1,0},{0,0,0,1,0,1},{0,0,1,1,0,1},
                            {0,0,0,1,1,0},{0,0,0,1,0,0},{0,0,1,1,0,0},
                            {1,0,0,1,1,0},{1,0,0,1,0,0},{1,0,1,1,0,0},
                            {0,0,0,0,1,1},{0,0,0,0,0,1},{0,0,1,0,0,1},
                            {0,0,0,0,1,0},{0,0,0,0,0,0},{0,0,1,0,0,0},
                            {1,0,0,0,1,0},{1,0,0,0,0,0},{1,0,1,0,0,0},
                            {0,1,0,0,1,1},{0,1,0,0,0,1},{0,1,1,0,0,1},
                            {0,1,0,0,1,0},{0,1,0,0,0,0},{0,1,1,0,0,0},
                            {1,1,0,0,1,0},{1,1,0,0,0,0},{1,1,1,0,0,0}};
#endif
